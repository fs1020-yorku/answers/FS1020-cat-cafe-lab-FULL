// 1a. Basic arrow function notation
const getCatName = () => {
	let catName = 'Tuna';
	console.log(`${catName}`);
};

console.log('getCatName Function');
getCatName();

//1b. Basic Function
function getCatProfile() {
	let catName = 'Tuna';
	let catBreed = 'Siamese';
	let catGender = 'Female';
	console.log(`${catName} ${catBreed} ${catGender}`);
}

console.log('getCatProfile Function');
getCatProfile();

// 2a. Cat Array
let cats = [
	{
		name: 'Tuna',
		breed: 'Siamese',
		gender: 'Female'
	},
	{
		name: 'Chester',
		breed: 'Tabby',
		gender: 'Male'
	},
	{
		name: 'Blue',
		breed: 'Naked',
		gender: 'Female'
	}
];

// 2b. Arrow function with for each to pull cat names from array
const getCatNamesArrowFunction = () => {
	cats.forEach((cat) => {
		console.log(cat.name);
	});
};

console.log('getCatNames Function');
getCatNamesArrowFunction();

// 2c. function with for each loop to pull cat names from array
function getCatNamesFunction() {
	cats.forEach((cat) => {
		console.log(cat.name);
	});
}

console.log('getCatNamesFunction Function');
getCatNamesFunction();
